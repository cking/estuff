"use strict"

const { app, BrowserWindow } = require("electron")

app.on("ready", () => {
    const win = new BrowserWindow({ width: 800, height: 600 })
    win.loadURL(`file://${__dirname}/render/index.html`)
})

app.on("window-all-closed", () => process.platform !== 'darwin' && app.quit())

