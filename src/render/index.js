"use strict"

import { h, render, Component } from "preact"

class Window extends Component {
    render() {
        return (
            <div style={{
                border: "5px solid red",
                textAlign: "center",
                padding: "3rem",
                color: "green"
            }}>
                yay, (p)react
            </div>
        )
    }
}

const el = document.getElementById("wrapper")

render(<Window />, el)
