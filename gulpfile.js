"use strict"

const path = require("path")
const gulp = require("gulp")

const electronDownloader = require('gulp-electron-downloader')
const rollup = require("rollup")

gulp.task("electron:download", done => electronDownloader(done))
gulp.task("electron:run", ["app:compile", "client:compile", "electron:download"], done => 
    require("child_process").exec(
        path.resolve("./electron/binaries/win32-x64/electron.exe") + " " + path.resolve("./dist/app"), 
        { cwd: "./dist" }, err => done(err)
    )
)

gulp.task("app:compile", done => {
    rollup.rollup({
        entry: "src/app/index.js",
        plugins: [
            require("rollup-plugin-node-resolve")({ jsnext: true, main: true }),
            require("rollup-plugin-commonjs")({
                include: 'node_modules/**',

                namedExports: {
                    "node_modules/electron/index.js": ["app", "BrowserWindow"]
                }
            }),
            require("rollup-plugin-buble")()
        ],
        external: id => id.indexOf("node_modules") >= 0,
    }).then(bundle => bundle.write({
        format: "cjs",
        dest: "dist/app/index.js"
    })).then(() => done()).catch(done)
})

gulp.task("client:compile", ["client:html"], done => {
    rollup.rollup({
        entry: "src/render/index.js",
        plugins: [
            require("rollup-plugin-node-resolve")({ jsnext: true, main: true, browser: true }),
            require("rollup-plugin-commonjs")({
                include: 'node_modules/**',

                namedExports: {
                    "node_modules/electron/index.js": ["app", "BrowserWindow"]
                }
            }),
            require("rollup-plugin-buble")({
                jsx: "h",
            })
        ],
        external: id => id.indexOf("node_modules") >= 0,
    }).then(bundle => bundle.write({
        format: "iife",
        moduleName: "render",
        dest: "dist/app/render/index.js"
    })).then(() => done()).catch(done)
})

gulp.task("client:html", () => gulp.src("src/index.html").pipe(gulp.dest("dist/app/render")))

gulp.task("default", ["electron:run"])